from snmproxyapi import SNMProxyAPI
import pprint

api = SNMProxyAPI(community="public")

example_get = api.get("anz0100-06a.tmn.scc.kit.edu", "1.3.6.1.2.1.1.1.0")
print("Get example response:")
pprint.pprint(example_get)

example_walk = api.walk("anz0100-06a.tmn.scc.kit.edu", "1.3.6.1.2.1.1")
print("Walk example response:")
pprint.pprint(example_walk)
