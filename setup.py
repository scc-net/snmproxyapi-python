#!/usr/bin/env python3

from setuptools import setup

setup(
    name = 'snmproxyapi',
    py_modules = ['snmproxyapi'],
    version = '0.2.0',
    description = 'Python module for access to the SNMProxy.',
    author='Dominik Rimpf',
    author_email='dominik.rimpf@partner.kit.edu',
    url='https://git.scc.kit.edu/scc-net/snmproxyapi-python',
    install_requires = [
        'requests',
    ],
    classifiers = [
        # Verhindert versehentliches hochladen auf öffentlichen Repos, z.B.
        # PyPi
        'Private :: Do not upload',
    ],
)
