import requests


class SNMProxyAPI(object):
    def __init__(self, community, server="snmp-proxy.tmn.scc.kit.edu", ssl_verify=True):
        self.server = server
        self.session = requests.Session()
        self.session.verify = ssl_verify
        self.community = str(community)

    def get(self, hostname, oid):
        resp = self.session.post("https://{server}/get/{hostname}/{oid}".format(
            server=self.server,
            hostname=hostname,
            oid=oid
        ), {"community": self.community})
        return resp.json()

    def walk(self, hostname, oid):
        resp = self.session.post("https://{server}/walk/{hostname}/{oid}".format(
            server=self.server,
            hostname=hostname,
            oid=oid
        ), {"community": self.community})
        return resp.json()
